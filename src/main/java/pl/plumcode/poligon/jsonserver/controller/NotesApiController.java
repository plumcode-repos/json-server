package pl.plumcode.poligon.jsonserver.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import org.springframework.web.bind.annotation.*;
import pl.plumcode.poligon.jsonserver.model.Note;
import pl.plumcode.poligon.jsonserver.repository.NotesRepository;

import java.text.DateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/notes")
public class NotesApiController {
    private ObjectMapper mapper;

    private NotesRepository notesRepository;
    NotesApiController(NotesRepository notesRepository){
        this.notesRepository = notesRepository;
        mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public String getAllNotes(
            @RequestParam(value = "title", required = false, defaultValue = "") String title,
            @RequestParam(value = "type", required = false, defaultValue = "") String type
    ){
        try {
            Iterable<Note> notes = notesRepository.findAll();

            if (!notes.iterator().hasNext())
                return "[]";

            Iterable<Note> filteredNotes;
            filteredNotes = () -> StreamSupport.stream(notes.spliterator(), false)
                    .filter(note -> title.isEmpty() || note.getTitle().matches(title))
                    .filter(note -> type.isEmpty() || note.getType().toString().matches(type))
                    .iterator();

            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(filteredNotes);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public String getNoteById(@PathVariable int id){
        try {
            Optional<Note> note = notesRepository.findById(id);

            if (!note.isPresent())
                return "";

            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(note.get());
        } catch (JsonProcessingException e){
            throw new RuntimeException(e);
        }
    }
}
