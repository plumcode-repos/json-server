package pl.plumcode.poligon.jsonserver.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/clients")
public class ClientsApiController {
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public Map<String, String> getAllNotes(){
        return Collections.singletonMap("list", "clients");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Map<String, Integer> getSingleNote(@PathVariable int id){
        return Collections.singletonMap("client id", id);
    }
}
