package pl.plumcode.poligon.jsonserver.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.plumcode.poligon.jsonserver.model.Note;
import java.util.Optional;

@Repository
public interface NotesRepository extends CrudRepository<Note, Integer> {
    Optional<Note> findById(Integer id);
    Optional<Note> findByTitle(String title);
    Optional<Note> findByType(int type);
}

