package pl.plumcode.poligon.jsonserver.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "notes")
public class Note {

    public enum NoteType {
        Urgent,
        Meeting,
        Personal
    }

    public enum NotePriority {
        Trivial,
        Minor,
        Major,
        Critical,
        Blocker
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(columnDefinition="nvarchar(50)")
    private String title;
    @Column(columnDefinition="nvarchar(max)")
    private String content;
    @Column(name = "createdon")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone="GMT+1")
    private Date createdOn;
    private Integer type;
//    private Date dueDate;
//    private NotePriority priority;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title.trim();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

//    public Date getDueDate() {
//        return dueDate;
//    }
//
//    public void setDueDate(Date dueDate) {
//        this.dueDate = dueDate;
//    }
//
//    public NotePriority getPriority() {
//        return priority;
//    }
//
//    public void setPriority(NotePriority priority) {
//        this.priority = priority;
//    }
//
//    public Note(String title) {
//        this.title = title;
//    }

    @Override
    public String toString() {
//        return "Note={id=" + id + ",title=" + title + ",content=" + content + ",createdOn=" + createdOn + ",type=" + type + ",dueDate=" + dueDate + ",priority=" + priority + "}";
        return "Note={id=" + id + ",title=" + title + ",content=" + content + ",createdOn=" + createdOn + ",type=" + type + "}";
    }
}
