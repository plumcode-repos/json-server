# JSON Server
for NotesApp

##Notes
Method|Description
------|-----------
`GET` `http://<hostname>:8080/notes`|Get all notes from repository. Optional parameters: `title`, `type`. Feel free to use regex!
`GET` `http://<hostname>:8080/notes\{id}`|get note of specified `{id}`

**Samples**

* `GET` `http://localhost:8080/notes?title=.*th%20note&type=%5B0-1%5D` - get notes that:
    * title ends with `th note`, e.g. _Four**th note**_,
    * type is `0` or `1`.
* `GET` `http://localhost:8080/notes/2` - get note with `id=2`.

##Regions
Method|Description
------|-----------

##Clients
Method|Description
------|-----------
